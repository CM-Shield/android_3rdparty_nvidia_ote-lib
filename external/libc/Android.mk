# Copyright (c) 2014, NVIDIA CORPORATION. All rights reserved
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libtlk_libc
LOCAL_MODULE_TAGS := optional
LOCAL_PRELINK_MODULE := false

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/include

LOCAL_SRC_FILES := \
	atexit.c \
	atoi.c \
	crtbegin.c \
	ctype.c \
	dlmalloc.c \
	libc_init_static.c \
	printf.c \
	qsort.c \
	sbrk.c \
	stackprotector.c \
	arch/arm/crtend.S \
	arch/arm/syscall.S \
	string/strlen.c \
	string/strnicmp.c \
	string/strcmp.c \
	string/strchr.c \
	string/strcpy.c \
	string/strncmp.c \
	string/memchr.c \
	string/memcmp.c \
	string/memcpy.c \
	string/memset.c \
	string/memmove.c

LOCAL_CFLAGS += -DTLK
LOCAL_CFLAGS += -nostdinc -nostdlib -fno-stack-protector

include $(BUILD_STATIC_LIBRARY)
