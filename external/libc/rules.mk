#
# Copyright (c) 2015 NVIDIA CORPORATION.  All Rights Reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

LOCAL_DIR := $(GET_LOCAL_DIR)

INCLUDES += \
	-I$(LOCAL_DIR)/include

MODULE_SRCS += \
	$(LOCAL_DIR)/atexit.c \
	$(LOCAL_DIR)/atoi.c \
	$(LOCAL_DIR)/crtbegin.c \
	$(LOCAL_DIR)/ctype.c \
	$(LOCAL_DIR)/dlmalloc.c \
	$(LOCAL_DIR)/libc_init_static.c \
	$(LOCAL_DIR)/printf.c \
	$(LOCAL_DIR)/qsort.c \
	$(LOCAL_DIR)/sbrk.c \
	$(LOCAL_DIR)/stackprotector.c \
	$(LOCAL_DIR)/arch/arm/crtend.S \
	$(LOCAL_DIR)/arch/arm/syscall.S \
	$(LOCAL_DIR)/string/strlen.c \
	$(LOCAL_DIR)/string/strnicmp.c \
	$(LOCAL_DIR)/string/strcmp.c \
	$(LOCAL_DIR)/string/strchr.c \
	$(LOCAL_DIR)/string/strcpy.c \
	$(LOCAL_DIR)/string/strncmp.c \
	$(LOCAL_DIR)/string/strerror.c \
	$(LOCAL_DIR)/string/strncpy.c \
	$(LOCAL_DIR)/string/strcat.c \
	$(LOCAL_DIR)/string/memchr.c \
	$(LOCAL_DIR)/string/memcmp.c \
	$(LOCAL_DIR)/string/memcpy.c \
	$(LOCAL_DIR)/string/memset.c \
	$(LOCAL_DIR)/string/memmove.c

MODULE_CFLAGS += -DTLK
MODULE_CFLAGS += -nostdinc -nostdlib -fno-stack-protector

include $(ROOT)/lib/make/module.mk
