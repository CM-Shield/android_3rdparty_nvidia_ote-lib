/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * @file
 * <b> NVIDIA Trusted Little Kernel Interface: Client Communications</b>
 *
 * @b Description: Declares the TLK client interface.
 */

/**
 *
 * @defgroup tlk_client Client Application Interface
 *
 * Defines the client application APIs.
 *
 * @ingroup ote_modules
 * @{
 */

#ifndef __OTE_CLIENT_H
#define __OTE_CLIENT_H

#include <common/ote_command.h>

#define TLK_DEVICE_BASE_NAME "tlk_device"
#define TE_IOCTL_MAGIC_NUMBER  ('t')

#define TE_IOCTL_OPEN_CLIENT_SESSION \
	_IOWR(TE_IOCTL_MAGIC_NUMBER, 0x10, union te_cmd)
#define TE_IOCTL_CLOSE_CLIENT_SESSION  \
	_IOWR(TE_IOCTL_MAGIC_NUMBER, 0x11, union te_cmd)
#define TE_IOCTL_LAUNCH_OP \
	_IOWR(TE_IOCTL_MAGIC_NUMBER, 0x14, union te_cmd)

/* secure storage ioctl */
#define TE_IOCTL_SS_CMD \
	_IOR(TE_IOCTL_MAGIC_NUMBER, 0x30, int)

#define TE_IOCTL_SS_CMD_GET_NEW_REQ	1
#define TE_IOCTL_SS_CMD_REQ_COMPLETE	2

/** Defines secure monitor calls (SMC) that clients use to communicate
    with trusted applications (TAs) in the secure world. */
enum {
	TLK_SMC_REQUEST = 0xFFFF1000,    /**< Requests OTE to launch a TA operation. */
	TLK_SMC_GET_MORE = 0xFFFF1001,   /**< Gets a pending answer without making new operation. */
	TLK_SMC_ANSWER = 0xFFFF1002,     /**< Answers from secure side. */
	TLK_SMC_NO_ANSWER = 0xFFFF1003,  /**< No answers for now (secure side idle). */
	TLK_SMC_OPEN_SESSION = 0xFFFF1004,
	TLK_SMC_CLOSE_SESSION = 0xFFFF1005,
};

/**
 * @defgroup tlk_client_app_com User Application Communication
 * @ingroup tlk_client
 * @{
 */

struct te_answer {
	uint32_t result;
	uint32_t session_id;
	uint32_t result_origin;
};


/**
 * Opens an open trusted environment (OTE) session.
 */
struct ote_opensession {
	te_service_id_t dest_service_id;
	te_operation_t operation;
	cmnptr_t answer;
};

/**
 * Closes an OTE session.
 */

struct ote_closesession {
	uint32_t session_id;
	cmnptr_t answer;
};

/**
 * Launches an operation request.
 */
struct ote_launchop {
	uint32_t session_id;
	te_operation_t operation;
	cmnptr_t answer;
};

union te_cmd {
	struct ote_opensession opensession;
	struct ote_closesession closesession;
	struct ote_launchop launchop;
};

/** @} <!-- tlk_client_app_com --> */

/**
 * @defgroup tlk_client_file_handling File Handling
 * @ingroup tlk_client
 * @{
 */

enum {
	OTE_FILE_REQ_TYPE_CREATE = 0x1,
	OTE_FILE_REQ_TYPE_DELETE = 0x2,
	OTE_FILE_REQ_TYPE_OPEN = 0x3,
	OTE_FILE_REQ_TYPE_CLOSE = 0x4,
	OTE_FILE_REQ_TYPE_READ = 0x5,
	OTE_FILE_REQ_TYPE_WRITE = 0x6,
	OTE_FILE_REQ_TYPE_GET_SIZE = 0x7,
	OTE_FILE_REQ_TYPE_SEEK = 0x8,
	OTE_FILE_REQ_TYPE_TRUNC = 0x9,
	OTE_FILE_REQ_TYPE_RPMB_WRITE = 0x1001,
	OTE_FILE_REQ_TYPE_RPMB_READ = 0x1002,
};

enum {
	OTE_FILE_REQ_FLAGS_ACCESS_RO	= 1,
	OTE_FILE_REQ_FLAGS_ACCESS_WO	= 2,
	OTE_FILE_REQ_FLAGS_ACCESS_RW	= 3,
};

enum {
	OTE_SEEK_WHENCE_SET	= 1,
	OTE_SEEK_WHENCE_CUR	= 2,
	OTE_SEEK_WHENCE_END	= 3,
};

#define OTE_RPMB_FRAME_SIZE	512

#define SS_OP_MAX_DATA_SIZE	0x1000
typedef struct {
	uint32_t	req_size;
	uint8_t		data[SS_OP_MAX_DATA_SIZE];
} ote_ss_op_t;
/** @} <!-- end of tlk_client_file_handling --> */

/** @} */

#endif
