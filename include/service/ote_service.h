/*
 * Copyright (c) 2013-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * @file
 * <b> NVIDIA Trusted Little Kernel Interface: Service Interface</b>
 *
 * @b Description: Declares data types and functions for
 *                 the TLK services.
 *
 */

/**
 * @defgroup tlk_services_interface Interface
 *
 * Defines Trusted Application (TA) services declarations and functions.
 *
 * @ingroup tlk_services
 *
 * @{
 */

#ifndef __OTE_SERVICE_H
#define __OTE_SERVICE_H

#include <sys/types.h>
#include <common/ote_common.h>
#include <common/ote_ioctl.h>

void te_exit_service(void);

enum {
	CREATE_INSTANCE		= 1UL,
	DESTROY_INSTANCE	= 2UL,
	OPEN_SESSION		= 3UL,
	CLOSE_SESSION		= 4UL,
	LAUNCH_OPERATION	= 5UL,
	TA_EVENT		= 6UL,
};

/**
 * Holds the layout of the \c te_oper_param_t structures which must
 * match the layout sent in by the non-secure (NS) world via the
 * TrustZone Secure Monitor Call (TZ SMC) path.
 */
typedef struct {
	uint32_t type;
	uint32_t session_id;
	uint32_t command_id;
	cmnptr_t params;
	uint32_t params_size;
	uint32_t dest_uuid[4];
	uint32_t result;
	uint32_t result_origin;
} te_request_t;

typedef struct {
	te_request_t request;
} te_ta_to_ta_request_args_t;

typedef struct {
	uint32_t   type;
	te_error_t result;
	cmnptr_t   context;
	uint32_t   command_id;
	cmnptr_t   params;
	uint32_t   params_size;
} te_entry_point_message_t;

/*
 * Service Interfaces
 */

/*! Initializes the service. */
te_error_t te_init(int argc, char **argv);

/*! Deinitializes the service. */
void te_destroy(void);

/*! Creates a new instance of the service. */
te_error_t te_create_instance_iface(void);

/*! Destroys an instance of the service. */
void te_destroy_instance_iface(void);

/*! Opens a session.
 *
 * \param sctx A pointer to the session.
 * \param oper A pointer to the operation.
 */
te_error_t te_open_session_iface(void **sctx, te_operation_t *oper);

/*! Closes an opened session.
 *
 * \param sctx A pointer to the session to close.
 */
void te_close_session_iface(void* sctx);

/*! Receives an operation.
 *
 * \param sctx A pointer to the session from which to receive
 *     the operation.
 * \param oper A pointer to the operation.
 */
te_error_t te_receive_operation_iface(void *sctx, te_operation_t *oper);

/*! Gets the instance context data. */
void* ote_get_instance_data(void);

/*! Sets an instance context data. */
void ote_set_instance_data(void*sessionContext);

/*! Holds the identity of a client/caller. */
typedef struct
{
	uint32_t login;
	te_service_id_t uuid;
} te_identity_t;

/*! Defines the supported login types. */
enum {
	TE_LOGIN_PUBLIC	= 0,
	TE_LOGIN_TA	= 7,
};

/*! Defines the type of property data. */
enum {
	TE_PROP_DATA_TYPE_UUID		= 1,
	TE_PROP_DATA_TYPE_IDENTITY	= 2,
};

/*! Defines the property data information. */
typedef enum {
	TE_PROPERTY_CURRENT_TA		= 0xFFFFFFFF,
	TE_PROPERTY_CURRENT_CLIENT	= 0xFFFFFFFE,
	TE_PROPERTY_TE_IMPLEMENTATION	= 0xFFFFFFFD,
} te_property_type_t;

/*! Holds data about the TA client. */
typedef struct {
	te_property_type_t prop;		/**< Holds the TE_PROPERTY_* value. */
	uint32_t data_type;			/**< Holds the data type of property. */
	union {
		te_service_id_t uuid;
		te_identity_t identity;
	} value;				/**< Holds the return value. */
	size_t value_size;			/**< Holds the size of return value. */
	te_error_t result;
} te_get_property_args_t;

/*!
 * Gets the service ID for the current Trusted Application (TA).
 *
 * \param[out] value A pointer to ::te_service_id_t, which holds the service ID.
 *
 * \retval OTE_SUCCESS Indicates the operation was successful.
 */
te_error_t te_get_current_ta_uuid(te_service_id_t *value);

/*!
 * Gets the current client's identity only if it is a secure TA.
 *
 * \param[out] value A pointer to ::te_identity_t, which holds the client's identity.
 *
 * \retval OTE_SUCCESS Indicates the operation was successful.
 */
te_error_t te_get_client_ta_identity(te_identity_t *value);

/*!
 * Gets the current client's identity.
 *
 * \param[out] value A pointer to ::te_identity_t, which holds the client's identity.
 *
 * \retval OTE_SUCCESS Indicates the operation was successful.
 */
te_error_t te_get_client_identity(te_identity_t *value);

#define DEVICE_UID_SIZE_BYTES		16

/*! Holds the device unique ID. */
typedef struct {
	uint8_t id[DEVICE_UID_SIZE_BYTES];
} te_device_unique_id;

/*! Gets the device unique ID.
 *
 * \param uid A pointer to the device unique ID.
 */
te_error_t te_get_device_unique_id(te_device_unique_id *uid);

/*! Holds the panic information. */
#define OTE_PANIC_MSG_MAX_SIZE	128
typedef struct {
	char msg[OTE_PANIC_MSG_MAX_SIZE+1];
} te_panic_args_t;

/*! Panics the system.
 *
 * This call does not return.
 *
 * \param args A pointer to panic arguments.
 */
void te_panic(char *msg) __attribute__ ((noreturn));

/*! Defines the maximum length of the "[task_name] " prefix for the te_fprintf() task log entries.
 */
#define OTE_TE_FPRINTF_PREFIX_MAX_LENGTH (OTE_TASK_NAME_MAX_LENGTH + 4)

/*! Set a printable prefix string that te_fprintf() outputs in front of
 * every log message from this task.
 *
 * The OTE library automatically sets a "[task_name] " log prefix based on the task name set
 * in the task manifest (if the manifest defines a task name).
 *
 * @param [in] prefix The string to use for the prefix or NULL for no prefix.
 *                    The maximum length of \a prefix is ::OTE_TE_FPRINTF_PREFIX_MAX_LENGTH.
 *                    A NULL value cancels the log prefix; a non-null string changes the prefix.
 */
void te_fprintf_set_prefix(const char *prefix);

/*! Prints the list of parameters for debugging.
*
* Prints the list of parameters with the parameter content.
*
* \param[in] param A pointer to a TLK operation.
*/
void te_oper_dump_param(te_oper_param_t *param);

/*! Prints the list of parameters for debugging.
*
* Prints the list of parameters with the parameter content.
*
* \param[in] te_op A pointer to a TLK operation.
*/
void te_oper_dump_param_list(te_operation_t *te_op);

enum ta_event_id {
	TA_EVENT_RESTORE_KEYS = 0,

	TA_EVENT_MASK = (1 << TA_EVENT_RESTORE_KEYS),
};

typedef struct {
	enum ta_event_id event_id;
} ta_event_args_t;

typedef te_error_t (*ta_event_handler_t)(ta_event_args_t *args);

/* Use events_mask to only register for selected events.  */
te_error_t te_register_ta_event_handler(ta_event_handler_t handler,
		uint32_t events_mask);

/** @} */

#endif
