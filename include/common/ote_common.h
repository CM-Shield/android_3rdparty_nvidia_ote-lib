/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * @file
 * <b> NVIDIA Trusted Little Kernel Interface: Common Declarations</b>
 *
 * @b Description: Declares the common declarations in the TLK interface.
 */

/**
 * @defgroup tlk_common_common Types
 *
 * Defines common data types and functions for Trusted Little Kernel (TLK).
 *
 * @ingroup ote_common
 *
 * @{
 */

#ifndef __OTE_COMMON_H
#define __OTE_COMMON_H

#include "compiler.h"
#include "ote_error.h"

/*! Defines the maximum length of a zero-terminated informative task name.
 */
#define OTE_TASK_NAME_MAX_LENGTH 24

/*! Defines the length of private data for the Trusted Application (TA).
 * The definition goes in the manifest.
 * The semantics of this optional data is defined per each TA.
 *
 * To hold an SHA1 digest, this definition must be at least 20 bytes.
 * Such a digest enables building a chain of trust to a TA with the manifest
 * data.
 *
 * This definition can be used to perform tasks such as:
 * - Loading public keys
 * - Loading X509 certificates and digests
 */
#define OTE_TASK_PRIVATE_DATA_LENGTH 20

/*! Defines a unique 16-byte ID for each TLK service. */
typedef struct {
	uint32_t time_low;
	uint16_t time_mid;
	uint16_t time_hi_and_version;
	uint8_t clock_seq_and_node[8];
} te_service_id_t;

/*! Specifies the operation object's parameter types. */
typedef enum {
	TE_PARAM_TYPE_NONE		= 0x0,
	TE_PARAM_TYPE_INT_RO		= 0x1,
	TE_PARAM_TYPE_INT_RW		= 0x2,
	TE_PARAM_TYPE_MEM_RO		= 0x3,
	TE_PARAM_TYPE_MEM_RW		= 0x4,
	TE_PARAM_TYPE_PERSIST_MEM_RO	= 0x100,
	TE_PARAM_TYPE_PERSIST_MEM_RW	= 0x101,
} te_oper_param_type_t;

/*! Holds a pointer large enough to support 32- and 64-bit clients. */
typedef uint64_t	cmnptr_t;

/*! Holds session information. */
typedef union {
	struct {
		uint32_t session_id;
		uint32_t context_id;
		te_result_origin_t result_origin;
	} client;
	struct {
		uint32_t session_id;
		te_result_origin_t result_origin;
	} service;
} te_session_t;

/*! Holds the operation object parameters. */
typedef struct {
	uint32_t index;
	te_oper_param_type_t type;
	union {
		struct {
			uint32_t val;
		} Int;
		struct {
			cmnptr_t base;
			uint32_t len;
		} Mem;
	} u;
	cmnptr_t next;
} te_oper_param_t;

/*! Holds operation object information that is to be
 * delivered to the TLK Secure Service.
 */
typedef struct {
	uint32_t command;
	te_error_t status;

	/** Holds pointers to the head/tail of the list
         * of \a param_t nodes.
	 */
	cmnptr_t list_head;
	cmnptr_t list_tail;

	uint32_t list_count;
	uint32_t interface_side;
} te_operation_t;

/*! Returns the origin of a returned result.
*
* Because it is possible for the operation to fail anywhere
* in the pipeline, this function returns the general block
* where the returned result originated.
*
* \param[in] session A valid session pointer.
*
* \return A \c te_result_origin_t ID number.
*/
te_result_origin_t te_get_result_origin(te_session_t *session);

/** @} */

enum {
	TE_CRITICAL	= 0,
	TE_ERR		= 1,
	TE_INFO		= 2,
	TE_SECURE	= 3,
	TE_INTERFACE	= 4,
	TE_RESULT	= 5,
};

/*!
 * For secure tasks:
 * Redirects prints to Trusted Little Kernel (TLK) writes.
 * This is a printf function for TLK services.
 *
 * For clients:
 * Prints to stdout.
 */
int te_fprintf(int fd, char *fmt, ...) __PRINTFLIKE(2,3);

#endif
