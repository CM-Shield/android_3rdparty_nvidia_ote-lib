/*
 * Copyright (c) 2013-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __OTE_STORAGE_PROTOCOL_H
#define __OTE_STORAGE_PROTOCOL_H

/*
 * Internal secure storage protocol declaration.
 */

enum {
	OTE_STORAGE_NS_CONFIG		= 0x1,
	OTE_STORAGE_FILE_CREATE		= 0x100,
	OTE_STORAGE_FILE_DELETE		= 0x101,
	OTE_STORAGE_FILE_OPEN		= 0x102,
	OTE_STORAGE_FILE_CLOSE		= 0x103,
	OTE_STORAGE_FILE_READ		= 0x104,
	OTE_STORAGE_FILE_WRITE		= 0x105,
	OTE_STORAGE_FILE_GET_SIZE	= 0x106,
	OTE_STORAGE_FILE_SEEK		= 0x107,
	OTE_STORAGE_FILE_TRUNC		= 0x108,
	OTE_STORAGE_FILE_TEST_EXIST	= 0x109,
	OTE_STORAGE_RPMB_READ		= 0x1001,
	OTE_STORAGE_RPMB_WRITE		= 0x1002,
	OTE_STORAGE_CPC_IO		= 0x1011,
};

/* configuration flags */
#define OTE_STORAGE_NS_CONFIG_FLAGS_RPMB_AVAILABLE	(0x0001)
#define OTE_STORAGE_NS_CONFIG_FLAGS_CPC_AVAILABLE	(0x0002)

#define OTE_MAX_DIR_NAME_LEN	(64)
#define OTE_MAX_FILE_NAME_LEN	(128)
#define OTE_MAX_DATA_SIZE	(2048)

typedef struct {
	char 		dname[OTE_MAX_DIR_NAME_LEN];
	char 		fname[OTE_MAX_FILE_NAME_LEN];
	uint32_t	flags;
} ote_file_create_params_t;

typedef struct {
	char 		dname[OTE_MAX_DIR_NAME_LEN];
	char 		fname[OTE_MAX_FILE_NAME_LEN];
} ote_file_delete_params_t;

typedef struct {
	char 		dname[OTE_MAX_DIR_NAME_LEN];
	char 		fname[OTE_MAX_FILE_NAME_LEN];
	uint32_t	flags;
	uint32_t	handle;
} ote_file_open_params_t;

typedef struct {
	uint32_t	handle;
} ote_file_close_params_t;

typedef struct {
	uint32_t	handle;
	uint32_t	data_size;
	char		data[OTE_MAX_DATA_SIZE];
} ote_file_write_params_t;

typedef struct {
	uint32_t	handle;
	uint32_t	data_size;
	char		data[OTE_MAX_DATA_SIZE];
} ote_file_read_params_t;

typedef struct {
	uint32_t	handle;
	uint32_t	size;
} ote_file_get_size_params_t;

typedef struct {
	uint32_t	handle;
	int32_t		offset;
} ote_file_seek_params_t;

typedef struct {
	uint32_t	handle;
	uint32_t	length;
} ote_file_trunc_params_t;

typedef struct {
	char 		dname[OTE_MAX_DIR_NAME_LEN];
	char 		fname[OTE_MAX_FILE_NAME_LEN];
} ote_file_test_exist_params_t;

/* size in bytes of RPMB frame */
#define OTE_RPMB_FRAME_SIZE	512

typedef struct {
	uint8_t		req_frame[OTE_RPMB_FRAME_SIZE];
	uint8_t		req_resp_frame[OTE_RPMB_FRAME_SIZE];
	uint8_t		resp_frame[OTE_RPMB_FRAME_SIZE];
} ote_rpmb_write_params_t;

typedef struct {
	uint8_t		req_frame[OTE_RPMB_FRAME_SIZE];
	uint8_t		resp_frame[OTE_RPMB_FRAME_SIZE];
} ote_rpmb_read_params_t;

/* size (with some padding) in bytes of CPC frame */
#define OTE_CPC_FRAME_SIZE	128

typedef struct {
	uint8_t		frame[OTE_CPC_FRAME_SIZE];
} ote_cpc_io_params_t;

typedef union {
	ote_file_create_params_t	f_create;
	ote_file_delete_params_t	f_delete;
	ote_file_open_params_t		f_open;
	ote_file_close_params_t		f_close;
	ote_file_read_params_t		f_read;
	ote_file_write_params_t		f_write;
	ote_file_get_size_params_t	f_getsize;
	ote_file_seek_params_t		f_seek;
	ote_file_trunc_params_t		f_trunc;
	ote_file_test_exist_params_t	f_test_exist;
	ote_rpmb_read_params_t		f_rpmb_read;
	ote_rpmb_write_params_t		f_rpmb_write;
	ote_cpc_io_params_t		f_cpc_io;
} ote_file_req_params_t;

/*
 * Holds a parameter block that is exchanged on each file system operation
 * request.
 */
typedef struct {
	uint32_t		req_size;
	uint32_t		type;
	te_error_t		result;
	uint32_t		params_size;
	ote_file_req_params_t	params;
} ote_ss_req_t;

#endif
