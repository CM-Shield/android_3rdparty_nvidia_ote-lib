/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>

#include <common/ote_error.h>
#include <service/ote_attrs.h>

te_error_t te_set_mem_attribute(te_attribute_t *attr, te_attribute_id_t id,
		void *buffer, uint32_t size)
{
	if ((id & OTE_ATTR_VAL) != 0) {
		return OTE_ERROR_BAD_FORMAT;
	}
	attr->id = id;
	attr->content.ref.buffer = buffer;
	attr->content.ref.size = size;

	return OTE_SUCCESS;
}

te_error_t te_get_mem_attribute_buffer(te_attribute_t *attr, void **ret)
{
	if (attr == NULL || (attr->id & OTE_ATTR_VAL) != 0) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	*ret = attr->content.ref.buffer;
	return OTE_SUCCESS;
}

te_error_t te_get_mem_attribute_size(te_attribute_t *attr, size_t *ret)
{
	if (attr == NULL || (attr->id & OTE_ATTR_VAL) != 0) {
		return OTE_ERROR_BAD_PARAMETERS;
	}

	*ret = attr->content.ref.size;
	return OTE_SUCCESS;
}

void te_copy_mem_attribute(void *buffer, te_attribute_t *key)
{
	memcpy(buffer, key->content.ref.buffer, key->content.ref.size);
}

te_error_t te_set_int_attribute(te_attribute_t *attr, te_attribute_id_t id,
		uint32_t a, uint32_t b)
{
	if ((id & OTE_ATTR_VAL) == 0) {
		return OTE_ERROR_BAD_FORMAT;
	}
	attr->id = id;
	attr->content.value.a = a;
	attr->content.value.b = b;

	return OTE_SUCCESS;
}

/* Free any internal attribute memory. */
void te_free_internal_attribute(te_attribute_t *attr)
{
	/* Do nothing for value attributes.  */
	if (attr->id & OTE_ATTR_VAL) {
		return;
	}
	/* Free buffer.  */
	free(attr->content.ref.buffer);
}

/* Copy data from one attribute to another.
 * A buffer for ref attribute types will be allocated.  */
te_error_t te_copy_attribute(te_attribute_t *dst, te_attribute_t *src)
{
	if (src->id & OTE_ATTR_VAL) {
		dst->id = src->id;
		dst->content.value.a = src->content.value.a;
		dst->content.value.b = src->content.value.b;
	} else {
		dst->content.ref.buffer = malloc(src->content.ref.size);
		if (dst->content.ref.buffer == NULL) {
			return OTE_ERROR_OUT_OF_MEMORY;
		}
		memcpy(dst->content.ref.buffer, src->content.ref.buffer,
				src->content.ref.size);
		dst->content.ref.size = src->content.ref.size;
		dst->id = src->id;
	}
	return OTE_SUCCESS;
}
