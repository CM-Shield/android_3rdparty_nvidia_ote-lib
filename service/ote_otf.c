/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>

#include <common/ote_common.h>
#include <common/ote_command.h>
#include <common/ote_nv_uuid.h>

#include <service/ote_service.h>
#include <service/ote_otf.h>
#include <service/ote_memory.h>

#define OTF_SET_AES_KEY		1
#define OTF_ERASE_AES_KEY	2
#define OTF_SET_AES_KEY_AT	3

static int otf_session_valid = 0;

te_error_t ote_otf_init(te_session_t **otfSession)
{
	te_service_id_t uuid = SERVICE_OTF_UUID;
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (otfSession == NULL)
		return OTE_ERROR_BAD_STATE;

	if (otf_session_valid == 1)
		return OTE_SUCCESS;

	te_init_operation(&operation);

	*otfSession = te_mem_calloc(sizeof(te_session_t));
	if (*otfSession != NULL) {
		/* open a OTF service session */
		err = te_open_session(*otfSession, &uuid, &operation);
		if (err != OTE_SUCCESS) {
			te_fprintf(TE_ERR,"%s: te_open_session failed with err (%d)\n",
					__func__, err);
			goto exit;
		}
		otf_session_valid = 1;
	}

exit:
	te_operation_reset(&operation);
	return err;
}

te_error_t ote_otf_deinit(te_session_t **otfSession)
{
	if (otfSession == NULL || *otfSession == NULL || !otf_session_valid)
		return OTE_ERROR_BAD_STATE;

	te_close_session(*otfSession);
	te_mem_free((void *)(*otfSession));
	otf_session_valid = 0;
	return OTE_SUCCESS;
}

te_error_t ote_otf_setkey(void *buffer, uint32_t len, uint32_t *keySlot,
		te_session_t *otfSession)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (!otfSession)
		return OTE_ERROR_BAD_STATE;

	if (!buffer)
		return OTE_ERROR_BAD_PARAMETERS;

	te_init_operation(&operation);

	te_oper_set_command(&operation, OTF_SET_AES_KEY);
	te_oper_set_param_mem_ro(&operation, 0, buffer, len);
	te_oper_set_param_int_rw(&operation, 1, *keySlot);

	err = te_launch_operation(otfSession, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed with err (%d)\n",
				__func__, err);
	}

	err = te_oper_get_param_int(&operation, 1, keySlot);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_oper_get_param_int failed with err (%d)\n",
				__func__, err);
	}

	te_operation_reset(&operation);
	return err;
}

te_error_t ote_otf_set_key_at(void *buffer, uint32_t len, uint32_t keySlot,
		te_session_t *otfSession)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (!otfSession)
		return OTE_ERROR_BAD_STATE;

	if (!buffer)
		return OTE_ERROR_BAD_PARAMETERS;

	te_init_operation(&operation);

	te_oper_set_command(&operation, OTF_SET_AES_KEY_AT);
	te_oper_set_param_mem_ro(&operation, 0, buffer, len);
	te_oper_set_param_int_rw(&operation, 1, keySlot);

	err = te_launch_operation(otfSession, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s failed with err (%d)\n", __func__, err);
	}

	te_operation_reset(&operation);
	return err;
}

te_error_t ote_otf_erasekey(te_session_t *otfSession)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (!otfSession)
		return OTE_ERROR_BAD_STATE;

	te_init_operation(&operation);
	te_oper_set_command(&operation, OTF_ERASE_AES_KEY);
	err = te_launch_operation(otfSession, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed with err (%d)\n",
				__func__, err);
	}

	te_operation_reset(&operation);
	return err;
}
