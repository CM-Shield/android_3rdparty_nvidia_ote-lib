#
# Copyright (c) 2015 NVIDIA CORPORATION.  All Rights Reserved.
#
# NVIDIA CORPORATION and its licensors retain all intellectual property
# and proprietary rights in and to this software, related documentation
# and any modifications thereto.  Any use, reproduction, disclosure or
# distribution of this software and related documentation without an express
# license agreement from NVIDIA CORPORATION is strictly prohibited.

LOCAL_DIR := $(GET_LOCAL_DIR)

LIB_ROOT := $(ROOT)/lib

# Enable CRITICAL/ERR prints by default. CRITICAL/ERR/INFO can be
# enabled by changing the value of DEBUG_LEVEL=2
MODULE_CFLAGS += -DDEBUG_LEVEL=1
MODULE_CFLAGS += -nostdinc -nostdlib -fno-stack-protector

INCLUDES += \
	-I$(LIB_ROOT)/include \
	-I$(LIB_ROOT)/external/libc/include \
	-I$(LIB_ROOT)/external/openssl/include

MODULE_SRCS += \
	$(LOCAL_DIR)/ote_attrs.c \
	$(LOCAL_DIR)/ote_command.c \
	$(LOCAL_DIR)/ote_crypto.c \
	$(LOCAL_DIR)/ote_memory.c \
	$(LOCAL_DIR)/ote_openssl.c \
	$(LOCAL_DIR)/ote_service.c \
	$(LOCAL_DIR)/ote_storage.c \
	$(LOCAL_DIR)/ote_otf.c \
	$(LOCAL_DIR)/ote_rtc.c \
	$(LOCAL_DIR)/ote_task_load.c \
	$(LOCAL_DIR)/ote_nvcrypto.c

include $(ROOT)/lib/make/module.mk
