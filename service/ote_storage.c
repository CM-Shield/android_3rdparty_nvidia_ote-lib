/*
 * Copyright (c) 2013-2014, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdlib.h>

#include <common/ote_error.h>
#include <common/ote_common.h>
#include <common/ote_command.h>
#include <common/ote_nv_uuid.h>
#include <common/ote_storage_protocol.h>

#include <service/ote_attrs.h>
#include <service/ote_memory.h>
#include <service/ote_storage.h>
#include <service/ote_service.h>

struct __te_storage_object {
	char *fname;
	uint32_t fname_len;
	uint32_t handle;
	te_session_t session;
};

typedef struct __te_storage_object *te_storage_object_t;

static te_session_t g_session;
static int g_session_ref;

static te_error_t init_storage_session(void)
{
	te_service_id_t uuid = SERVICE_STORAGE_UUID;
	te_operation_t operation;
	te_error_t err;

	te_init_operation(&operation);
	err = te_open_session(&g_session, &uuid, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "tee_open_session failed with err (0x%x)\n", err);
		return err;
	}
	return OTE_SUCCESS;
}

static te_error_t get_storage_session(te_session_t *session)
{
	te_error_t err;

	if (!g_session_ref) {
		err = init_storage_session();
		if (err != OTE_SUCCESS) {
			return err;
		}
	}

	g_session_ref++;
	*session = g_session;

	return OTE_SUCCESS;
}

static void put_storage_session(te_session_t *session)
{
	if (!g_session_ref) {
		te_fprintf(TE_ERR, "%s: bad refcount\n", __func__);
		return;
	}

	if (g_session.client.session_id != session->client.session_id) {
		te_fprintf(TE_ERR, "%s: bad session\n", __func__);
		return;
	}

	g_session_ref--;

	if (!g_session_ref)
		te_close_session(&g_session);
}

te_error_t te_open_storage_object(char *name, te_storage_flags_t flags,
				te_storage_object_t *obj)
{
	uint32_t namelen;
	te_storage_object_t o = NULL;
	te_session_t session;
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	/* open a storage service session */
	err = get_storage_session(&session);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "tee_open_session failed with err (0x%x)\n", err);
		return err;
	}

	namelen = strlen(name) + 1;

	/* issue file open request */
	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_OPEN);
	te_oper_set_param_mem_ro(&operation, 0, name, namelen);
	te_oper_set_param_int_rw(&operation, 1, flags);
	te_oper_set_param_int_rw(&operation, 2, 0);
	err = te_launch_operation(&session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);
		goto exit;
	}

	o = te_mem_calloc(sizeof(*o));
	if (o == NULL) {
		te_fprintf(TE_ERR, "%s: out of memory\n", __func__);
		err = OTE_ERROR_OUT_OF_MEMORY;
		goto exit;
	}

	o->fname = (char *)te_mem_alloc(namelen);
	if (o->fname == NULL) {
		te_fprintf(TE_ERR, "%s: out of memory\n", __func__);
		err = OTE_ERROR_OUT_OF_MEMORY;
		goto exit;
	}
	te_mem_move(o->fname, name, namelen);
	o->fname_len = namelen;

	err = te_oper_get_param_int(&operation, 2, &o->handle);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_oper_get_param_int failed: 0x%x\n",
			__func__, err);
		goto exit;
	}

	o->session = session;

	*obj = o;

exit:
	if (err != OTE_SUCCESS) {
		if (o) {
			if (o->fname)
				te_mem_free(o->fname);
			te_mem_free(o);
		}
		put_storage_session(&session);
	}
	te_operation_reset(&operation);

	return err;
}

te_error_t te_close_storage_object(te_storage_object_t obj)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	/* issue file close request */
	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_CLOSE);
	te_oper_set_param_int_ro(&operation, 0, obj->handle);

	err = te_launch_operation(&obj->session, &operation);

	te_operation_reset(&operation);
	put_storage_session(&obj->session);

	te_mem_free(obj->fname);
	te_mem_free(obj);

	return err;
}

te_error_t te_create_storage_object(char *name, te_storage_flags_t flags,
				te_storage_object_t *obj)
{
	te_session_t session;
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	/* open a storage service session */
	err = get_storage_session(&session);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "te_open_session failed with err (0x%x)\n", err);
		return err;
	}

	/* issue file create request */
	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_CREATE);
	te_oper_set_param_mem_ro(&operation, 0, name, strlen(name) + 1);
	te_oper_set_param_int_ro(&operation, 1, flags);
	err = te_launch_operation(&session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);
		te_operation_reset(&operation);
		put_storage_session(&session);
		return err;
	}

	te_operation_reset(&operation);
	put_storage_session(&session);

	/* issue file open if asked to do so */
	if (obj) {
		err = te_open_storage_object(name, flags, obj);
		return err;
	}

	return OTE_SUCCESS;
}

te_error_t te_delete_storage_object(te_storage_object_t obj)
{
	te_session_t session;
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	/* open a storage service session */
	err = get_storage_session(&session);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "te_open_session failed with err (0x%x)\n", err);
		return err;
	}

	/* issue file delete request */
	te_init_operation(&operation);
	te_oper_set_param_mem_ro(&operation, 0, obj->fname, obj->fname_len);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_DELETE);
	err = te_launch_operation(&session, &operation);
	if (err != OTE_SUCCESS)
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);

	te_operation_reset(&operation);
	put_storage_session(&session);

	return err;
}

te_error_t te_delete_named_storage_object(const char *name)
{
	te_session_t session;
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	/* open a storage service session */
	err = get_storage_session(&session);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "te_open_session failed with err (0x%x)\n", err);
		return err;
	}

	/* issue file delete request */
	te_init_operation(&operation);
	te_oper_set_param_mem_ro(&operation, 0, name, strlen(name) + 1);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_DELETE);
	err = te_launch_operation(&session, &operation);
	if (err != OTE_SUCCESS)
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);

	te_operation_reset(&operation);
	put_storage_session(&session);

	return err;
}

te_error_t te_read_storage_object(te_storage_object_t obj, void *buffer,
		uint32_t size, uint32_t *count)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (!buffer || !count)
		return OTE_ERROR_BAD_PARAMETERS;

	/* issue file read request */
	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_READ);
	te_oper_set_param_int_ro(&operation, 0, obj->handle);
	te_oper_set_param_mem_rw(&operation, 1, buffer, size);
	te_oper_set_param_int_rw(&operation, 2, 0);
	err = te_launch_operation(&obj->session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);
		*count = 0;
		goto exit;
	}

	/* get number of bytes read */
	err = te_oper_get_param_int(&operation, 2, count);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_oper_get_param_int failed: 0x%x\n",
			__func__, err);
		*count = 0;
		goto exit;
	}

exit:
	te_operation_reset(&operation);

	return err;
}

te_error_t te_write_storage_object(te_storage_object_t obj, const void *buffer,
		uint32_t size)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	if (!buffer || size == 0) {
		te_fprintf(TE_ERR, "%s: bad param: buffer %p size 0x%x\n",
			__func__, buffer, size);
		return OTE_ERROR_BAD_PARAMETERS;
	}

	/* issue file write request */
	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_WRITE);
	te_oper_set_param_int_ro(&operation, 0, obj->handle);
	te_oper_set_param_mem_ro(&operation, 1, buffer, size);
	err = te_launch_operation(&obj->session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);
	}

	te_operation_reset(&operation);

	return err;
}

te_error_t te_get_storage_object_size(te_storage_object_t obj, uint32_t *size)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;
	uint32_t value_a = 0;

	if (!size)
		return OTE_ERROR_BAD_PARAMETERS;

	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_GET_SIZE);
	te_oper_set_param_int_ro(&operation, 0, obj->handle);
	te_oper_set_param_int_rw(&operation, 1, 0);

	err = te_launch_operation(&obj->session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);
		goto exit;
	}

	err = te_oper_get_param_int(&operation, 1, &value_a);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_oper_get_param_int failed: 0x%x\n",
			__func__, err);
		goto exit;
	}

exit:
	*size = value_a;

	te_operation_reset(&operation);

	return err;
}


te_error_t te_seek_storage_object(te_storage_object_t obj, int32_t offset,
				te_storage_whence_t whence)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	/* issue file seek request */
	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_SEEK);
	te_oper_set_param_int_ro(&operation, 0, obj->handle);
	te_oper_set_param_int_ro(&operation, 1, offset);
	te_oper_set_param_int_ro(&operation, 2, whence);

	err = te_launch_operation(&obj->session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);
	}

	te_operation_reset(&operation);

	return err;
}

te_error_t te_trunc_storage_object(te_storage_object_t obj, uint32_t size)
{
	te_operation_t operation;
	te_error_t err = OTE_SUCCESS;

	/* truncate file */
	te_init_operation(&operation);
	te_oper_set_command(&operation, OTE_STORAGE_FILE_TRUNC);
	te_oper_set_param_int_ro(&operation, 0, obj->handle);
	te_oper_set_param_int_ro(&operation, 1, size);
	err = te_launch_operation(&obj->session, &operation);
	if (err != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: te_launch_operation failed: 0x%x\n",
			__func__, err);
	}

	te_operation_reset(&operation);

	return err;
}
