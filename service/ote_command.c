/*
 * Copyright (c) 2013, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/ioccom.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include <common/ote_common.h>
#include <common/ote_error.h>
#include <service/ote_service.h>

te_error_t check_error(int error)
{
	te_error_t ret_error = OTE_SUCCESS;
	switch (error) {
		case ENOMEM:
			ret_error = OTE_ERROR_OUT_OF_MEMORY;
			break;
		default:
			ret_error = OTE_ERROR_COMMUNICATION;
			break;
	}
	return ret_error;
}

te_error_t te_open_session(te_session_t *session, te_service_id_t *service,
		te_operation_t *operation)
{
	te_ta_to_ta_request_args_t args;

	memset(&args, 0, sizeof(args));

	args.request.type = OPEN_SESSION;
	args.request.params_size = operation->list_count;
	args.request.params = operation->list_head;

	memcpy(&args.request.dest_uuid, service,
			sizeof(args.request.dest_uuid));

	if (ioctl(1, OTE_IOCTL_TA_TO_TA_REQUEST, &args) < 0) {
		session->service.result_origin = OTE_RESULT_ORIGIN_API;
		args.request.result = check_error(errno);
		te_fprintf(TE_ERR, "%s: failed to open session: errno=%d, result=%d\n",
				__func__, errno, args.request.result);
		goto exit;
	} else {
		session->service.result_origin = args.request.result_origin;
	}

	session->service.session_id = args.request.session_id;

exit:
	return args.request.result;
}

void te_close_session(te_session_t *session)
{
	te_ta_to_ta_request_args_t args;

	memset(&args, 0, sizeof(args));

	args.request.type = CLOSE_SESSION;
	args.request.session_id = session->service.session_id;

	if (ioctl(1, OTE_IOCTL_TA_TO_TA_REQUEST, &args) < 0) {
		args.request.result = check_error(errno);
		te_fprintf(TE_ERR, "%s: failed to close session id: id=%d, errno=%d, result=%d\n",
			__func__, session->service.session_id, errno, args.request.result);
		goto exit;
	}

exit:
	return;
}

te_error_t te_launch_operation(te_session_t *session, te_operation_t *operation)
{
	te_ta_to_ta_request_args_t args;

	if (operation->status != OTE_SUCCESS) {
		te_fprintf(TE_ERR, "%s: operation already failed, status 0x%x\n",
			__func__, operation->status);
		return operation->status;
	}

	memset(&args, 0, sizeof(args));

	args.request.type = LAUNCH_OPERATION;
	args.request.session_id = session->service.session_id;
	args.request.params_size = operation->list_count;
	args.request.params = operation->list_head;
	args.request.command_id = operation->command;

	if (ioctl(1, OTE_IOCTL_TA_TO_TA_REQUEST, &args) < 0) {
		session->service.result_origin = OTE_RESULT_ORIGIN_API;
		args.request.result = check_error(errno);
		te_fprintf(TE_ERR, "%s: failed to launch operation: errno=%d, result=%d\n",
			__func__, errno, args.request.result);
		goto exit;
	} else {
		session->service.result_origin = args.request.result_origin;
	}

exit:
	return args.request.result;
}

te_result_origin_t te_get_result_origin(te_session_t *session)
{
	return session->service.result_origin;
}
