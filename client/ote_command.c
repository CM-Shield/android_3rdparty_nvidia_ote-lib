/*
 * Copyright (c) 2013-2015, NVIDIA CORPORATION. All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>

#include <client/ote_client.h>

te_error_t check_error(int error)
{
	te_error_t ret_error = OTE_SUCCESS;
	switch (error) {
		case ENOMEM:
			ret_error = OTE_ERROR_OUT_OF_MEMORY;
			break;
		default:
			ret_error = OTE_ERROR_COMMUNICATION;
			break;
	}
	return ret_error;
}

te_error_t te_open_session(te_session_t *session, te_service_id_t *service,
		te_operation_t *operation)
{
	union te_cmd cmd;
	int ret = 0;
	struct te_answer answer;

	char devicename[64];
	int dev_fd = 0;

	strcpy(devicename, "/dev/");
	strcat(devicename, TLK_DEVICE_BASE_NAME);

	dev_fd = open(devicename, O_RDWR, 0);
	if (dev_fd < 0) {
		session->client.result_origin = OTE_RESULT_ORIGIN_API;
		printf("Failed to open device (%s) errno=%d\n",
				devicename, errno);
		answer.result = OTE_ERROR_BUSY;
		goto exit;
	}

	memset(&cmd, 0, sizeof(union te_cmd));
	memset(&answer, 0, sizeof(struct te_answer));

	if (operation)
		memcpy(&(cmd.opensession.operation), operation,
				sizeof(te_operation_t));

	memcpy(&(cmd.opensession.dest_service_id), service,
			sizeof(te_service_id_t));
	cmd.opensession.answer = (cmnptr_t)(uintptr_t) &answer;

	ret = ioctl(dev_fd, TE_IOCTL_OPEN_CLIENT_SESSION, &cmd);
	if (ret != 0) {
		session->client.result_origin = OTE_RESULT_ORIGIN_API;
		answer.result = check_error(errno);
		printf("Failed to open session. errno=%d\n, result=%d\n",
				errno, answer.result);
		close(dev_fd);
		goto exit;
	} else {
		session->client.result_origin = answer.result_origin;
	}

	session->client.session_id = answer.session_id;
	session->client.context_id = dev_fd;

exit:
	return answer.result;
}

void te_close_session(te_session_t *session)
{
	union te_cmd cmd;
	struct te_answer answer;
	int ret = 0;

	memset(&cmd, 0, sizeof(union te_cmd));
	memset(&answer, 0, sizeof(struct te_answer));

	cmd.closesession.session_id = session->client.session_id;
	cmd.closesession.answer = (cmnptr_t)(uintptr_t) &answer;

	ret = ioctl(session->client.context_id, TE_IOCTL_CLOSE_CLIENT_SESSION,
			&cmd);
	if (ret != 0) {
		answer.result = check_error(errno);
		printf("Failed to close session id. errno=%d, result=%d\n",
				errno, answer.result);
		goto exit;
	}

	if (!session->client.context_id)
		return;

	close(session->client.context_id);
	session->client.context_id = 0;
exit:
	return;
}

te_error_t te_launch_operation(te_session_t *session, te_operation_t *operation)
{
	int ret = 0;
	union te_cmd cmd;
	struct te_answer answer;

	memset(&cmd, 0, sizeof(union te_cmd));
	memset(&answer, 0, sizeof(struct te_answer));

	if (operation)
		memcpy(&(cmd.launchop.operation), operation,
				sizeof(te_operation_t));

	cmd.launchop.session_id = session->client.session_id;
	cmd.launchop.answer = (cmnptr_t)(uintptr_t) &answer;

	ret = ioctl(session->client.context_id, TE_IOCTL_LAUNCH_OP, &cmd);
	if (ret < 0) {
		session->client.result_origin = OTE_RESULT_ORIGIN_API;
		answer.result = check_error(errno);
		printf("Failed to launch operation. errno=%d, result=%d\n",
				errno, answer.result);
	} else {
		session->client.result_origin = answer.result_origin;
	}

	return answer.result;
}

te_result_origin_t te_get_result_origin(te_session_t *session)
{
	return session->client.result_origin;
}

int te_fprintf(int fd, char *fmt, ...)
{
	va_list ap;

	int ret;

	/* Sanity check */
	if (fd < 0 || fd > TE_SECURE)
		return -1;

	va_start(ap, fmt);
	ret = vprintf(fmt, ap);
	va_end(ap);

	return ret;
}
